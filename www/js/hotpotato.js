function hot_request(url, success, error) {
    const req = new XMLHttpRequest();

    const _url = `http://casino-proto.herokuapp.com/api/${url}`;

    console.log(`Accediendo a: ${_url}`);
    
    req.open("GET", _url, true);

    req.onloadend = () => {
        if (req.status === 404) {
            if (error) error("No se puede acceder");
            return;
        }
        
        if (req.status === 400) {
            if (error) error(req.responseText);
            return;
        }

        if (success) success(JSON.parse(req.responseText));
    };

    req.send(null);
}

function hot_location(path){
    window.location.href = path;
}

function open_scanner() {   
    QRScanner.prepare((err, status) => {
        console.log(err);
        if (err) {
            return;
        }
        console.log(status);

        QRScanner.show((status) => {
            console.log(status);

            QRScanner.scan((err, text) => {
                if (err) {
                    alert("Error al escanear");
                    return;
                }

                QRScanner.hide();
                alert(text)

                //sessionStorage.setItem("scanner/code:new", text);

                //window.location.href = "menu.html";
            });
        });
    });
}