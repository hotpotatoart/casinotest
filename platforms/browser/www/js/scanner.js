document.addEventListener("deviceready", () => {
    open_scanner();
});

function open_scanner() {
    QRScanner.prepare((err, status) => {
        console.log("prepare");
        QRScanner.show((status) => {
            console.log("Sow");
            QRScanner.scan((err, text) => {
                console.log("scan");
                if (err) {
                    alert("Error al escanear");
                    return;
                }
                console.log(text);
                const scan_modal = document.getElementById("scan_modal");
                scan_modal.hidden = false;                
            });
        });
    });
}

function stopScanner(){
    QRScanner.hide(function(status){
        alert("Deteniendo scaner; " status);
    });
}