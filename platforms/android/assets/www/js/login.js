function login() 
{
    const code = document.getElementById("txt-code").value;

    const btn = document.querySelector("button");

    console.log(btn);

    btn.disabled = true;
    
    hot_request(`login/${code}`, user => 
	{
        console.log(user);
        btn.disabled = false;
		window.location.href = 'menu.html'; // Enviar a Menu Principal;
    }, err => 
	{
        console.log(err);
        btn.disabled = false;
        alert(err);
    });
}