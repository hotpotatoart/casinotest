const sizeWindow = {
    width: 360,
    height: 580
};
let SidebarIsVisible = true;
let onAnimation = false;
let userInfo;
const indexFriends = {
    first: 0,
    last: 4
};

let Sidebar = null;

window.addEventListener("load", () => {
    createSideBar();
    Sidebar = document.getElementById("sidebar");
    sizeWindow.width = document.documentElement.clientWidth;
    sizeWindow.height = document.documentElement.clientHeight;
    // adjustBg();
    adjustSidebar();
    getInfoUser();
    //showSidebar();
});

function createSideBar() {
    const div = document.createElement("div");

    div.id = "sidebar";

    div.innerHTML = `
        <div id="divCloseSidebar" align="right">
            <img id="imgClose" onclick="showSidebar()" src="sidebar/img/close.png">
        </div>         
        <div id="userInfo" align="center">
            <img id="imgUser" src="">
            <div id="userName"></div>
            <div id="creditsUser">1000 creditos</div>
        </div>   
        <div id="titleFriends" align="center">Amigos</div>  
        <div id="UserOfFriends" class="containerOfSidebar" align="center">
            <div id="pictureOfFriends"></div>          
        </div>
        <div id="titleShareCode" align="center">Tu código</div>
        <div id="userShareCode" class="containerOfSidebar" align="center">
            <div id="txtShareCode">Código</div><br>
        </div>        

        <div id="titlePrizes" align="center">Premios</div> 
        <div id="prizesOfUser" class="containerOfSidebar" align="center">
            <div id="prizeSolo" class="prizeBox">
                Individual <br> 3/9 <br>
                <img class="imgPrize" src="sidebar/img/Iphone.png">
                <div id="prizeName">Iphone X</div>
            </div>          
            <div id="prizeGrupo" class="prizeBox">
                Grupal <br> 12/54 <br>
                <img class="imgPrize" src="sidebar/img/ViajeParis.png">
                <div id="prizeName">Viaje a Paris</div>
            </div>
            <br><br>
        </div>
    `;

    document.body.appendChild(div);
}

window.onresize = () => {
    sizeWindow.width = document.documentElement.clientWidth;
    sizeWindow.height = document.documentElement.clientHeight;
    // adjustBg();
    adjustSidebar();
};

function adjustFriends() {
    const friendsBox = document.getElementById("pictureOfFriends");
    const friendList = userInfo.friends;
    let div = '';
    for (let i in friendList) {
        div += `<div id="friend_${i}" class="friendBox">` + `<img id="imgFriend_${i}" class="imgsFriends" ` + `src="${friendList[i].picture}">` + `<div id="friendName_${i}">${friendList[i]
            .name
            .split(' ')[0]}</div>` + `</div>`;
    }
    friendsBox.innerHTML = div + "<br><br>";
} // fin de la funcion adjustFriends

function adjustUserInfo() {
    const imgUser = document.getElementById("imgUser");
    const txtUserName = document.getElementById("userName");
    imgUser.src = userInfo.picture;
    txtUserName.innerHTML = `${userInfo.name}`;
    const txtShareCode = document.getElementById("txtShareCode");
    txtShareCode.innerHTML = `${userInfo.share_code}`;
    adjustFriends();
} // fin de la funcion adjustUserInfo

function showSidebar() {    
    if (!onAnimation) {
        onAnimation = true;
        if (SidebarIsVisible) {
            Sidebar.style.animationName = 'showSidebar';
            setTimeout(() => {
                Sidebar.style.marginLeft = '-5%';
                SidebarIsVisible = !SidebarIsVisible;
                onAnimation = false;
            }, 900);
        } else {
            Sidebar.style.animationName = 'hideSidebar';
            setTimeout(() => {
                Sidebar.style.marginLeft = '-100%';
                SidebarIsVisible = !SidebarIsVisible;
                onAnimation = false;
            }, 900);
        }
    }
} // fin de la funcion showSidebar

function adjustSidebar() {
    Sidebar.style.width = `${sizeWindow.width * 3 / 4}px`;
} // fin de la funcion adjustSidebar

function adjustBg() {
    const body = document.getElementsByClassName("bgProfileSidebar")[0];
    body.style.backgroundSize = `${sizeWindow.width}px ${sizeWindow.height}px`;
} // fin de la funcion adjustBg

function getInfoUser() {
    const url = "batman";
    hot_request(url, user => {
        console.log(user);
        userInfo = user;
        adjustUserInfo();
    }, err => {
        console.log(err);
        alert(err);
    });
} // fin de la funcion get info user
