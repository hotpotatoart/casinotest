let isGrupal = false;
let prizes = {};

window.onload = ()=>{
    getPrizes();

};


function showDetailPrize(i, isInd){
    if(isInd){
        alert("Premio: " + prizes.solo[i].description );
    } else {
        alert("Premio: " + prizes.team[i].description );
    }
    
}

function showPrizes(isInd){
    const boxPrizes = document.getElementById("boxPrizes");   
    let prizeList = [];
    if(isInd){
        prizeList = prizes.solo;       
    } else {        
        prizeList = prizes.team;
    }
         
    let div = '';
    for(let i in prizeList){
        div += 
        `<div id="boxPrize_${i}" class="boxPrize" align="center">
            <table id="containerPrize_${i}" class="containerPrize" border="0px">    
                <tr align="center">
                    <td rowspan="2" style="width: 30%;">
                        <img id="imgPrize_${i}" class="imgPrize" src="${prizeList[i].picture}" width="80%">
                    </td>
                    <td id="prizeName_${i}" align="left">                       
                        <br>
                        <div id="txtNamePrize_${i}" class="txtNamePrize">
                            ${prizeList[i].description}                            
                        </div>
                        <br>
                    </td>                    
                <tr align="center">                   
                    <td id="imgInfo_${i}" class="imgInfo" align="right">
                        <img onclick="showDetailPrize(${i}, ${isInd})" src="img/btnInfo.png" width="18%">
                    </td>  
                </tr>    
            </table>            
        </div> `;        
    }    
    boxPrizes.innerHTML = div;
}

function swichImg(elmt){
    isGrupal = !isGrupal;
    if(isGrupal){
        elmt.src = "img/btnGrup.png";
        showPrizes(false);
    }else{
        elmt.src = "img/btnInd.png";
        showPrizes(true);
    }
}

function getPrizes() {
    //const code = document.getElementById("txt-code").value;

    hot_request(`prizes/all`, prizesR => {
        console.log(prizesR);
        prizes = prizesR;
        showPrizes(true);// true = individuales, false = grupales
    }, err => {
        console.log(err);
        alert(err);
    });
}